import random
import string
lineage = ["nth", "1st", "2nd", "3rd", "4th", "5th", "6th", "7th", "8th", "9th", "10th", "11th", "12th", "13th", "14th", "15th", "16th", "17th", "18th", "19th", "20th", "21st"]
titles = ["master chief", "general", "dragonborn", "inquisitor", "don", "killer", "Nighthawk", "prince", "princess", "king", "queen", "ArchMonarch", "Mr", "Ms", "Mrs", "Mx", "Mistress", "Master", "Emperor", "Gunner", "ArchWitch", "Empress", "Emprex", "Sir", "lady", "knight", "Paladin", "Battlemage", "Archmage", "Dragonkiller", "Agent", "Shogun", "Gundam", "Darth", "von", "kaiser","Doctor","Valkerie"]
names = ["hell","cake","jorg","scud","scrunt", "fang", "truck", "sky", "gods", "god", "jaw", "missile", "rocket", "hammer", "load", "shark", "stab", "anvil", "torpedo", "harpoon", "tornado", "duct", "scold", "shatter", "butt", "crush", "buff", "kevlar", "cable", "biff", "ass", "kill", "bot", "arm", "bicep", "grip", "duke", "tron" , "chin", "bacon", "rex", "hemorrhage", "calibre", "buck", "guitar", "bass", "drum", "snare", "vice", "burger", "burgler", "juggernaut", "turbo", "pilot", "rough", "squeeze", "shave", "flash", "blood", "bleed", "concrete", "slab", "gym", "brick", "girder", "rhino", "tumble", "bruise", "beard", "fist", "pump", "fisto", "stomp", "boot", "kick", "axe", "thrust", "butt", "ram", "horn", "meat", "torque", "flame", "beast", "mug", "hack", "carbon", "ace", "pectoral", "raven", "hawk", "drop", "dog", "battle", "skull", "spine", "wolf", "tiger", "lion", "leopard", "jet", "jaguar", "sixpack", "break", "iron", "boot", "scream", "yell", "steel", "tungsten", "blast", "splode", "deadlift", "dead", "lift", "bench", "slash", "piledriver", "gun", "cannon", "laser", "bust", "nut", "metal", "gear", "bull", "solid", "fuck", "doom", "fridge", "ham", "crunch", "beef", "muscle", "slam", "dirk", "stallion", "valkerie", "amazon", "chest", "slice", "diamond", "rock", "crusher", "trucker", "killer", "squeezer", "fucker", "guitarist", "bassist", "shitter", "drummer", "shaver", "pumper", "stabber", "killer", "slammer", "stormer", "puncher", "stomper", "kicker", "shield" ]
suffix = ["smith", "jaeger", "son", "daughter","cake","hell"]
actions = ["puncher", "kicker", "slayer", "exploder", "imploder", "eater", "headbutter", "smasher"]
things = ["skeleton", "house", "groin", "tooth", "vehicle", "bone", "anvil", "plank", "log", "brick", "block", "mountain", "tree", ]
toughName = ""
def nthName():
    n = "The " + random.choice(lineage)
    return n

def coin():
    c = random.randint(1, 2)
    return c

def dice():
    d = random.randint(1, 6)
    return d
layout = dice()

def titleVar():
    t = random.choice(titles)
    return t

def suffixVar():
    sv = random.choice(suffix)
    return sv

linVarCh = coin()

def linVar():
    if (linVarCh == 2):
        lv = nthName
    else:
        lv = ""
    return lv

sc = coin()

def tc(n):
    if (layout == 1):
        n = n + random.choice(names) + random.choice(names)
    elif (layout == 2):
        n = n + random.choice(names) + " " + random.choice(names) + random.choice(names)
    elif (layout == 3):
        n = n + random.choice(names) + random.choice(names) + " " + random.choice(names) + random.choice(names)
    elif (layout == 4):
        n = n + random.choice(names) + random.choice(names) + " " + random.choice(names) + " " + random.choice(names) + random.choice(names)#d
    elif (layout == 5):
        n = n + random.choice(names) + random.choice(names) + " " + random.choice(names)
    elif (layout == 6):
        n = n + random.choice(names) + " " + random.choice(names)
    return n

def sf(n):
    if (sc == 1):
        tnf = n + suffixVar
        return tnf

def doer():
    a = "The " + random.choice(things) + " " + random.choice(actions)
    return a

toughName = tc(toughName)

linc = dice()

if (linc == 1):
    toughName = toughName + " " + nthName()
elif (linc == 2):
    toughName = toughName + " " + nthName() + ": " + doer()

print("Y O U R   T O U G H   N A M E   I S:   \n" + str(toughName))
toughName = toughName + "\n"
f = open('log.txt', 'a')
with open('log.txt', 'a') as logfile:
    logfile.write(toughName)
input()
